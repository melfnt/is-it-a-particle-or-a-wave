
export class Point3D {
    constructor (public x: number, public y: number, public z: number) {}

    public copy () { return new Point3D (this.x, this.y, this.z); }

    public dot (other: Point3D) {
        return this.x * other.x + this.y * other.y + this.z * other.z;
    }

    public sum (other:Point3D) {
        return new Point3D (this.x + other.x, this.y + other.y, this.z + other.z);
    }
    
    public inv () {
        return new Point3D (-this.x, -this.y, -this.z);
    }
    
    public times ( d: number ) {
        return new Point3D (d*this.x, d*this.y, d*this.z);
    }

    public squared_distance (other: Point3D) {
        return (this.x-other.x)**2 + (this.y-other.y)**2 + (this.z-other.z)**2;
    }
}

export function circle_circle_intersection (A: {center:{x:number, y:number, z:number}, radius:number}, B: {center:{x:number, y:number, z:number}, radius:number}): {x:number, y:number, z:number}[] {
    const d = Math.abs (A.center.x - B.center.x);
    const f = (A.radius**2 - B.radius**2 + d**2)/(2*d);
    const h = Math.sqrt (A.radius**2 - f**2);
    return [
        {x:A.center.x + f, y:A.center.y+h, z:A.center.z},
        {x:A.center.x + f, y:A.center.y-h, z:A.center.z},
    ]
}
