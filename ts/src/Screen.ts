
import { rgb_string_to_object } from './utils';
import { Point3D } from './geometry';
import { Buffers } from './rendering';
import { initPolygonBuffer, initSphereBuffers } from './shapes';

export class Screen {

    private color: {r: number, g: number, b:number, a: number};
    private bgcolor: {r: number, g: number, b:number, a: number};
    private total_esposure_time = 0;
    private current_values: number[] = [];
    private cached_stripes_buffers: Buffers[] = [];
    private cached_spots_buffers: Buffers[] = [];
    private spots: {x: number, y:number, z:number}[] = [];
    private invalid_cache = false;

    constructor ( color_str: string, bgcolor_str: string, private center: Point3D, private width: number, 
                  private height: number, private xstep: number, private threshold: number, 
                  private spots_radius: number, private refresh_time: number, private halo_max_size: number,
                  private halo_height_smooth: number ) {
        this.color = rgb_string_to_object (color_str);
        this.bgcolor = rgb_string_to_object (bgcolor_str);
        for (let x=0; x<width; x+=xstep) {
            this.current_values.push (0);
        }
    }

    public update (values: (number|undefined)[]) {
        for (let j=0; j<this.current_values.length; ++j) {
            const v = values[j];
            if (v!==undefined && v>this.threshold && this.current_values[j] == 0) {
                this.current_values[j] = 1;
                this.invalid_cache = true;
            }
        }
        ++this.total_esposure_time;
        return this.invalid_cache || values.some(x => x===undefined);
    }

    public get_buffers (gl: WebGLRenderingContext | WebGL2RenderingContext): Buffers[] {
        if (this.invalid_cache && this.total_esposure_time % this.refresh_time == 0) {
            this.recompute_cached_stripes (gl);
            this.invalid_cache = false;
            // console.debug ("[SCREEN] recompute stripes. this.values:", this.current_values);
        }
        this.cached_spots_buffers.push (...this.spots.map ( s => initSphereBuffers (gl, s, this.spots_radius, this.color ) ));
        this.spots = [];
        return this.cached_stripes_buffers.concat (this.cached_spots_buffers);
    }

    public add_spot (p: {x:number, y?:number, z:number}) {
        if (p.y === undefined) {
            if (this.xmin <= p.x && p.x <= this.xmax && this.zmin <= p.z && p.z <= this.zmax) {
                this.spots.push ( {...p, y: this.center.y} );
            }
        }
        else {
            this.spots.push ( {...p, y: p.y === undefined ? this.center.y : p.y } );
        }
    }

    private recompute_cached_stripes (gl: WebGLRenderingContext | WebGL2RenderingContext) {
        this.cached_stripes_buffers = [];
        const stripes_indices: number[] = [];
        let plateau_start = -1;
        for (let j=1; j<this.current_values.length-1; ++j) {
            if (this.current_values[j-1] == 0 && this.current_values[j] == 1 && this.current_values[j+1] == 0) {
                stripes_indices.push (j);
            }
            else if (this.current_values[j-1] == 0 && this.current_values[j] == 1) {
                plateau_start = j;
            }
            else if (this.current_values[j-1] == 1 && this.current_values[j] == 0) {
                stripes_indices.push (Math.trunc((plateau_start + j)/2));
            } 
        }

        const center = this.current_values.length/2;
        stripes_indices.forEach ( j => {
            const d = Math.abs (j-center)/center;
            const halo_stripes = (1-d) * this.halo_max_size / this.xstep;
            const intensity_center = 1-d;
            const intensity_step = intensity_center/halo_stripes;
            const xj = this.center.x + j/this.current_values.length*this.width - this.width/2;
            const h = this.height;
            const hstep = (this.height/this.halo_height_smooth) / halo_stripes;
            const stripe_color = {
                r: intensity_center*this.color.r + (1-intensity_center)*this.bgcolor.r,
                g: intensity_center*this.color.g + (1-intensity_center)*this.bgcolor.g,
                b: intensity_center*this.color.b + (1-intensity_center)*this.bgcolor.b,
                a: 1
            }
            this.cached_stripes_buffers.push ( initPolygonBuffer (
                gl,
                [{x: xj-this.xstep/2, y: this.center.y, z: this.center.z+h/2}, {x: xj-this.xstep/2, y: this.center.y, z: this.center.z-h/2}, {x: xj+this.xstep/2, y: this.center.y, z: this.center.z-h/2}, {x: xj+this.xstep/2, y: this.center.y, z: this.center.z+h/2}],
                [stripe_color, stripe_color, stripe_color, stripe_color],
                true
            ));
            for (let i=0, t=intensity_center-intensity_step, h=this.height; i<halo_stripes-1; ++i, t-=intensity_step, h-=hstep) {
                const stripe_color = {
                    r: t*this.color.r + (1-t)*this.bgcolor.r,
                    g: t*this.color.g + (1-t)*this.bgcolor.g,
                    b: t*this.color.b + (1-t)*this.bgcolor.b,
                    a: 1
                }
                let x = xj+i*this.xstep;
                this.cached_stripes_buffers.push ( initPolygonBuffer (
                    gl,
                    [{x: x-this.xstep/2, y: this.center.y, z: this.center.z+h/2}, {x: x-this.xstep/2, y: this.center.y, z: this.center.z-h/2}, {x: x+this.xstep/2, y: this.center.y, z: this.center.z-h/2}, {x: x+this.xstep/2, y: this.center.y, z: this.center.z+h/2}],
                    [stripe_color, stripe_color, stripe_color, stripe_color],
                    true
                ));
                x = xj-i*this.xstep;
                this.cached_stripes_buffers.push ( initPolygonBuffer (
                    gl,
                    [{x: x-this.xstep/2, y: this.center.y, z: this.center.z+h/2}, {x: x-this.xstep/2, y: this.center.y, z: this.center.z-h/2}, {x: x+this.xstep/2, y: this.center.y, z: this.center.z-h/2}, {x: x+this.xstep/2, y: this.center.y, z: this.center.z+h/2}],
                    [stripe_color, stripe_color, stripe_color, stripe_color],
                    true
                ));
            }
        });

    }

    get xmin () {
        return this.center.x - this.width/2;
    }
    
    get xmax () {
        return this.center.x + this.width/2;
    }
    
    get zmin () {
        return this.center.z - this.height/2;
    }
    
    get zmax () {
        return this.center.z + this.height/2;
    }

    get y () {
        return this.center.y;
    }
    
    get w () {
        return this.width;
    }
    
    get h () {
        return this.height;
    }

}
