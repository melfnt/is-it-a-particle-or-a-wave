import { mat4, ReadonlyVec3 } from "gl-matrix";

export class Controls3D {

    readonly translation_factor = 0.05;
    readonly rotation_factor = 10*Math.PI/180;
    readonly scale_factor = 0.01;
    
    readonly key_to_offset_move: {[key: string]: ReadonlyVec3} = 
    {
        'q': [                       0,                        0,  this.translation_factor],
        'w': [                       0, -this.translation_factor,                        0],
        'e': [                       0,                        0, -this.translation_factor],
        'a': [ this.translation_factor,                        0,                        0],
        's': [                       0,  this.translation_factor,                        0],
        'd': [-this.translation_factor,                        0,                        0],
    }
    readonly key_to_offset_rotate: {[key: string]: ReadonlyVec3} = 
    {
        'u': [ 0,  0,  1],
        'i': [ 0, -1,  0],
        'o': [ 0,  0, -1],
        'j': [ 1,  0,  0],
        'k': [ 0,  1,  0],
        'l': [-1,  0,  0],
    }
    readonly key_to_offset_scale: {[key: string]: ReadonlyVec3} = 
    {
        'z': [ 1+this.scale_factor, 1+this.scale_factor, 1+this.scale_factor ],
        'x': [ 1-this.scale_factor, 1-this.scale_factor, 1-this.scale_factor ],
    }

    private w2v = mat4.create();

    constructor (element: HTMLElement) {
        this.reset ();
        element.addEventListener ("keypress", this.on_key_pressed.bind(this));
    }

    get modelViewMatrix() {
        return this.w2v;
    }

    private reset () {
        this.w2v = mat4.create();
        this.w2v = [
            2.5,
            0,
            0,
            0,
            0,
            2.1650636196136475,
            -1.25,
            0,
            0,
            1.25,
            2.1650636196136475,
            0,
            0,
            -1.9497597217559814,
            -5.8125,
            1
        ]
        // mat4.translate(this.w2v, this.w2v, [-0.0, -2, -6.0]);
        // mat4.rotate(this.w2v, this.w2v, -45 * Math.PI / 180, [1,0.2,0.4]);
        // mat4.scale(this.w2v, this.w2v, [2.5,2.5,2.5]);
        // console.debug ("reset w2v matrix:", mat4.str (this.w2v));

    }

    private on_key_pressed (evt: KeyboardEvent) {
        // console.log ("Event keypressed:", evt);
        const pressed = evt.key.toLowerCase();

        if ( pressed==="r" ) this.reset(); 

        if ( pressed in this.key_to_offset_move ) {
            const offsets = this.key_to_offset_move[pressed];
            mat4.translate(this.w2v, this.w2v, offsets);
        }
        if ( pressed in this.key_to_offset_rotate ) {
            const offsets = this.key_to_offset_rotate[pressed];
            mat4.rotate(this.w2v, this.w2v, this.rotation_factor, offsets);
        }
        if ( pressed in this.key_to_offset_scale ) {
            const offsets = this.key_to_offset_scale[pressed];
            mat4.scale(this.w2v, this.w2v, offsets);
        }

    }

}
