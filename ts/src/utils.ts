
export function rgb_string_to_object ( inp: string, alpha=1 ) {
    // console.log ("rgb_string_to_object input:", inp);
    if (inp.length != 7) throw new Error ("string to be converted into object must have the form #rrggbb. Invalid input "+inp);
    const r = parseInt ( inp.slice(1,3), 16 )/255;
    const g = parseInt ( inp.slice(3,5), 16 )/255;
    const b = parseInt ( inp.slice(5,7), 16 )/255;
    return {r,g,b, a:alpha};
}

export function copy_array ( a: any[] ): any[] {
    return Object.assign ([], a);
}

export function random_gauss (mean: number, std: number) {    
    const u = 1 - Math.random();
    const v = 1 - Math.random();
    return mean + Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v ) * std;
}
