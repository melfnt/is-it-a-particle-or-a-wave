
export interface MetaParameter {
    min?: number;
    max?: number;
}

export interface SimulationParameters {
    // ENVIRONMENTS
    walls_color: string;
    background_color: string;
    
    // BALLS
    balls_color: string;
    balls_accel: number;
    balls_radius: number;
    balls_base_speed_x: number;
    balls_base_speed_y: number;
    balls_base_speed_z: number;

    // WALLS POSITIONS
    wall_1_pos_x: number;
    wall_1_pos_y: number;
    wall_1_pos_z: number;
    wall_2_pos_x: number;
    wall_2_pos_y: number;
    wall_2_pos_z: number;
    wall_3_pos_x: number;
    wall_3_pos_y: number;
    wall_3_pos_z: number;
    wall_4_pos_x: number;
    wall_4_pos_y: number;
    wall_4_pos_z: number;
    wall_5_pos_x: number;
    wall_5_pos_y: number;
    wall_5_pos_z: number;
    wall_6_pos_x: number;
    wall_6_pos_y: number;
    wall_6_pos_z: number;
    wall_7_pos_x: number;
    wall_7_pos_y: number;
    wall_7_pos_z: number;

    wall_1_lx: number;
    wall_1_ly: number;
    wall_1_lz: number;
    wall_2_lx: number;
    wall_2_ly: number;
    wall_2_lz: number;
    wall_3_lx: number;
    wall_3_ly: number;
    wall_3_lz: number;
    wall_4_lx: number;
    wall_4_ly: number;
    wall_4_lz: number;
    wall_5_lx: number;
    wall_5_ly: number;
    wall_5_lz: number;
    wall_6_lx: number;
    wall_6_ly: number;
    wall_6_lz: number;
    wall_7_lx: number;
    wall_7_ly: number;
    wall_7_lz: number;

    baseboard_height: number;

    // BALLS AND PHOTONS CONE
    cone_radius: number;

    // LAUNCHING PAD POSITION
    launching_pad_pos_x: number;
    launching_pad_pos_y: number;
    launching_pad_pos_z: number;

    //WAVES
    waves_speed: number;
    waves_color: string;
    waves_wavelength: number;
    waves_max_distance: number;

    //LIGHT
    light_color: string;
    
    //LIGHT
    electrons_per_second: number;
    electrons_color: string;
    electrons_stripes_distance: number;
    electrons_stripes_std_factor: number;

    //AUTOMATIC FIRE
    automatic_fire_balls_per_second: number;

    //SCREEN DRAWING
    screen_sampling_x: number;
    screen_threshold: number;
    screen_spot_radius: number;
    screen_refresh_time: number;
    screen_halo_size: number;
    screen_stop_updating_after_how_many_refreshes_not_required: number;
    screen_height: number;
    screen_halo_smooth: number;

    [name: string]: string | number;
}

export const default_parameters: SimulationParameters = {"electrons_per_second":100,"electrons_stripes_std_factor":0.05,"electrons_stripes_distance":0.6,"electrons_color":"#FFFFFF","light_color":"#FFFFFF","walls_color":"#2DB95E","background_color":"#755676","balls_color":"#daff75","balls_accel":0.000001,"balls_radius":0.005,"balls_base_speed_x":0,"balls_base_speed_y":0.0001,"balls_base_speed_z":0,"wall_1_pos_x":0.91,"wall_1_pos_y":0.4,"wall_1_pos_z":0.2,"wall_2_pos_x":-0.91,"wall_2_pos_y":0.4,"wall_2_pos_z":0.2,"wall_3_pos_x":0,"wall_3_pos_y":1.2,"wall_3_pos_z":0.2,"wall_4_pos_x":0.955,"wall_4_pos_y":0.4,"wall_4_pos_z":0.2,"wall_5_pos_x":0,"wall_5_pos_y":0.4,"wall_5_pos_z":0.2,"wall_6_pos_x":-0.955,"wall_6_pos_y":0.4,"wall_6_pos_z":0.2,"wall_7_pos_x":0,"wall_7_pos_y":1.2,"wall_7_pos_z":0.2,"wall_1_lx":1.8,"wall_1_ly":0.05,"wall_1_lz":0.5,"wall_2_lx":1.8,"wall_2_ly":0.05,"wall_2_lz":0.5,"wall_3_lx":10,"wall_3_ly":0.05,"wall_3_lz":1.6,"wall_4_lx":1.71,"wall_4_ly":0.05,"wall_4_lz":0.5,"wall_5_lx":0.18,"wall_5_ly":0.05,"wall_5_lz":0.5,"wall_6_lx":1.71,"wall_6_ly":0.05,"wall_6_lz":0.5,"wall_7_lx":10,"wall_7_ly":0.05,"wall_7_lz":1.6,"cone_radius":0.5,"launching_pad_pos_x":0,"launching_pad_pos_y":0,"launching_pad_pos_z":0.2,"automatic_fire_balls_per_second":100,"waves_speed":0.0002,"waves_color":"#CCCCFF","waves_wavelength":0.05,"baseboard_height":0.04,"screen_sampling_x":0.01,"screen_threshold":1.1,"screen_spot_radius":0.002,"waves_max_distance":3,"screen_refresh_time":10,"screen_halo_size":0.1,"screen_height":0.5,"screen_stop_updating_after_how_many_refreshes_not_required":10,"screen_halo_smooth":16};

const default_metaparameters: {[name: string]: MetaParameter} = {
    balls_accel: {min: 0.00000001, max: 0.0001}
}

export interface Parametrizable {
    parameters_changed_callback (pars: SimulationParameters): void;
}

export class ParametersManager {

    private _parameters: SimulationParameters = default_parameters;
    private _metaparameters = default_metaparameters;
    private subscribers: Parametrizable[] = [];
    
    constructor (private interface_parent: HTMLElement, private json_element: HTMLInputElement) {
        json_element.addEventListener ('change', (evt: Event) => {
            this._parameters = JSON.parse ((<any>evt.target).value);
            // console.log("json parameters changed: new parameters", this._parameters);
            this.create_parameters_interface ();
            this.subscribers.forEach (s => s.parameters_changed_callback (this._parameters) );
        })

        json_element.value = JSON.stringify (this._parameters);

        this.create_parameters_interface ();
    }

    public register ( p: Parametrizable ) {
        this.subscribers.push (p);
        p.parameters_changed_callback (this._parameters);
    }

    public unregister ( p: Parametrizable ) {
        const idx = this.subscribers.indexOf (p);
        if (idx < 0) throw new Error ("parametrizable to unregister must be registered first");
        this.subscribers.splice (idx, 1);
    }

    public parameter_changed ( name: string, new_value: string ) {
        if (Object.keys (this._parameters).indexOf (name) !== undefined) {
            if (typeof (this._parameters[name]) == "number") {
                this._parameters[name] = +new_value;
            }
            else {
                this._parameters[name] = new_value;
            }
            this.subscribers.forEach (s => s.parameters_changed_callback (this._parameters) );
        }
    }

    get parameters () {
        return {"parameters": this._parameters, "meta": this._metaparameters};
    }

    set parameters (arg) {
        this._parameters = arg.parameters;
    }

    private parameter_changed_from_interface (evt: Event) {
        const name = (<any>evt.target).name;
        const value = (<any>evt.target).value;
        this.parameter_changed (name, value);
        const other_elements = document.getElementsByName (name);
        if (other_elements.length > 1) {
          other_elements.forEach ( e => (<HTMLInputElement>e).value = value );
        }
        this.json_element.value = JSON.stringify (this._parameters);
    }
      
    private create_parameters_interface () {
  
        this.interface_parent.innerHTML = "";

        // <div class="parameters-group">
        //   <label class="parameter-chunk" >balls acceleration:</label> <input class="parameter parameter-chunk" name="balls_accel"> <input type="range" min="0" max="0.5" step="0.001" class="parameter parameter-chunk" name="balls_accel">
        // </div>
        
        let regex = new RegExp("_", "g");
      
        for (let name of Object.keys(this._parameters).sort()) {
          const group = document.createElement ("div");
          group.classList.add ("parameters-group");
          this.interface_parent.appendChild (group);
      
          const label = document.createElement ("label");
          label.classList.add ("parameter-chunk");
          label.innerText = name.replace (regex, " ", ) + ":";
          group.appendChild (label);    
      
          const input = document.createElement ("input");
          input.name = name;
          input.addEventListener ("change", this.parameter_changed_from_interface.bind(this));
          input.value = "" + this._parameters[name];
          group.appendChild (input);    
      
          if (name.search("color") >= 0) input.type = "color";
      
          const info = this._metaparameters[name];
          if (info) {
            const input2 = document.createElement ("input");
            input2.name = name;
            input2.addEventListener ("change", this.parameter_changed_from_interface.bind(this));
            input2.value = "" + this._parameters[name];
            input2.type = "range";
            const low = info.min || 0;
            const high = info.max || 100;
            input2.min = ""+low;
            input2.max = ""+high;
            input2.step = "any";
            group.appendChild (input2);
      
          }
      
        }
      }
}
