import { Point3D, circle_circle_intersection } from './geometry';
import { Buffers } from './rendering';
import { Screen } from './Screen';
import { initParallelepipedBuffers, initSphereBuffers, initLimitedCircleBuffers, initConeBuffer } from './shapes';
import { Parametrizable, SimulationParameters, default_parameters } from './parameters';
import { rgb_string_to_object, copy_array, random_gauss } from './utils';

export interface Simulation extends Parametrizable {
    readonly buffers: Buffers[];
    readonly fire_callback: Function;
    tick (deltat: number): void;
    reset(): void;
};

enum WaveVisualizationMode {
    CRESTS = "crests",
    FULL = "full"
}

class Wall {

    constructor (readonly position: Point3D, readonly dimensions: Point3D) {}

}

class Ball {

    readonly _EPS = 1e-10;

    // acceleration coefficient per millisecond
    
    private delta_speed: Point3D;
    private prev_position: Point3D;
    private cached_collision_point: Point3D | null;

    constructor (readonly position: Point3D, private speed: Point3D, readonly radius: number, walls:Wall[]=[], private acceleration = 0.000001) {
        const t = speed.x + speed.y + speed.z;
        this.delta_speed = new Point3D (0,0,0);
        if ( t!=0 ) {
            this.delta_speed = new Point3D ( 
                this.acceleration * speed.x/t,
                this.acceleration * speed.y/t,
                this.acceleration * speed.z/t
            );
        }
        this.prev_position = position.copy();
        this.cached_collision_point = walls.map ( 
            w => this.compute_collision_point (w) ).reduce ( (a: Point3D | null, b) => {
                if (!b) return a;
                if (!a) return b;
                const dist_a = (a.x- position.x) ** 2 + (a.y- position.y) ** 2 + (a.z- position.z) ** 2
                const dist_b = (b.x- position.x) ** 2 + (b.y- position.y) ** 2 + (b.z- position.z) ** 2
                return dist_a < dist_b ? a : b;
            }, null );
        // console.log ("collision point:", this.cached_collision_point);
    };

    public tick ( deltat: number ) {
        this.prev_position = this.position.copy ();
        this.position.x += deltat * this.speed.x;
        this.position.y += deltat * this.speed.y;
        this.position.z += deltat * this.speed.z;
        this.speed.x += this.delta_speed.x * deltat;
        this.speed.y += this.delta_speed.y * deltat;
        this.speed.z += this.delta_speed.z * deltat;
    }

    private compute_collision_point ( w: Wall ): Point3D | null {
        const l = this.speed;
        const l0 = this.position;
        const p0 = new Point3D(w.position.x, w.position.y - w.dimensions.y/2, w.position.z)
        const n = new Point3D (0,-1,0);
        const denominator = l.dot (n);
        
        if (Math.abs(denominator) < this._EPS) return null;
        
        const d = p0.sum ( l0.inv() ).dot (n) / denominator;
        const ip = l0.sum ( l.times(d) );
        if ( w.position.x - w.dimensions.x/2 < ip.x && ip.x < w.position.x + w.dimensions.x/2 
            && w.position.z - w.dimensions.z/2 < ip.z && ip.z < w.position.z + w.dimensions.z/2
        ) {
            ip.y = w.position.y - w.dimensions.y/2;
            return ip;
        }
        
        return null;
    }

    public collided () {
        if (!this.cached_collision_point) return null;
        const prev = this.prev_position;
        const pos = this.position;
        const coll = this.cached_collision_point;
        // const dx = Math.abs (prev.x - pos.x);
        // const dy = Math.abs (prev.y - pos.y);
        // const dz = Math.abs (prev.z - pos.z);
        const dx = Math.abs (prev.x - pos.x) + this._EPS;
        const dy = Math.abs (prev.y - pos.y) + this._EPS;
        const dz = Math.abs (prev.z - pos.z) + this._EPS;
        if ( Math.abs (prev.x-coll.x) < dx && Math.abs (pos.x-coll.x) < dx && 
             Math.abs (prev.y-coll.y) < dy && Math.abs (pos.y-coll.y) < dy && 
             Math.abs (prev.z-coll.z) < dz && Math.abs (pos.z-coll.z) < dz
        ) return this.cached_collision_point;
        return null;
    }

}

interface Crest {
    center: {x: number, y:number, z: number},
    radius: number,
    ymin: number,
    ymax: number,
}

class WaveTree {

    // nearest point reached by the wave
    private _a=0;
    // farthest point reached by the wave 
    private _b=0;
    // waves generated when this wave hits a slit
    private children: {w: WaveTree, s: Point3D}[] = []; 

    private ymin: number;
    private ymax: number;
    private ybreaks_for_children: number[];

    constructor ( private center: Point3D, private speed: number, private wavelenght: number,
                  private slits: Point3D[], ybreaks: number[], private max_travel_distance: number) {
        if (ybreaks.length < 2) throw new Error ("there must be at least 2 ybreaks");
        this.ymin = ybreaks[0];
        this.ymax = ybreaks[1];
        this.ybreaks_for_children = ybreaks.slice (1);
    }

    public tick ( deltat: number, generation: boolean ): boolean {
        this._b += deltat*this.speed;
        if ( this._b > this.max_travel_distance + this.wavelenght ) {
            this._b = this.max_travel_distance + (this._b - this.max_travel_distance) % this.wavelenght;
        }
        if (!generation) {
            this._a += deltat*this.speed;
        }
        let all_children_are_dead = true;
        this.children.forEach ( child => {
            const generate_child = this.center.squared_distance (child.s) > this._a**2;
            const dead = child.w.tick (deltat, generate_child);
            all_children_are_dead = all_children_are_dead && dead;
        });
        for (let j=this.slits.length-1; j>=0; --j) {
            const slit = this.slits[j];
            // if a slit is reached remove it and create a child wave with the next ybreaks and the slits past its origin
            if ( this.center.squared_distance(slit) <= this._b**2 ) {
                const slits_for_child = this.slits.filter ( s=> s.y > this.ybreaks_for_children[0] );
                this.children.push ({
                    w: new WaveTree (slit, this.speed, this.wavelenght, slits_for_child, this.ybreaks_for_children, this.max_travel_distance),
                    s: slit
                });
                this.slits.splice (j, 1);
            }
        }
        return this.center.y+this._a > this.ymax && all_children_are_dead;
    }

    get crests (): Crest[] {
        const out: Crest[] = [];
        for (let c = this._b - this.wavelenght/4; c>=this._a; c = c-this.wavelenght)
        {
            out.push ({center: this.center, radius: c, ymin: this.ymin, ymax: this.ymax});
        }
        return out.concat ( ...this.children.map (child => child.w.crests) );
    }

    public get_line_values ( y: number, xmin: number, xmax: number, xstep: number ): (number|undefined)[] {
        const out = Array<number|undefined> (Math.trunc((xmax-xmin)/xstep)).fill (undefined);
        if (this.ymin <= y && y <= this.ymax) {
            const omega = 2 * Math.PI/this.wavelenght;
            for (let x=xmin,j=0; x<=xmax; x+=xstep,++j) {
                const dist = Math.sqrt ( (x - this.center.x)**2 + (y - this.center.y)**2 );
                const alpha = (1 - dist/this.max_travel_distance);
                if (this._a <= dist && dist <= this._b) {
                    out[j] = alpha * Math.sin ( omega * (this._b - dist) );
                }
            }
        }
        else if ( y > this.ymax ) {
            this.children.forEach ( child => {
                const cv = child.w.get_line_values (y, xmin, xmax, xstep);
                for (let j=0; j<out.length; ++j) {
                    out[j] = (out[j] === undefined && cv[j] === undefined) ? undefined : ((out[j] || 0) + (cv[j] || 0));
                } 
            });          
        }
        return out;
    }
}

function compute_walls_depending_on_slits (number_of_slits: number, parameters: SimulationParameters, screen_color: string, radius_screen_limit?: number): {walls: Wall[], screen: Screen, debugScreen: Screen} {
    const EPS = 1e-4;
    if (number_of_slits == 1) {
        const w1_pos = new Point3D ( parameters.wall_1_pos_x, parameters.wall_1_pos_y, parameters.wall_1_pos_z );  
        const w2_pos = new Point3D ( parameters.wall_2_pos_x, parameters.wall_2_pos_y, parameters.wall_2_pos_z );  
        const w3_pos = new Point3D ( parameters.wall_3_pos_x, parameters.wall_3_pos_y, parameters.wall_3_pos_z );  
        const w1_len = new Point3D ( parameters.wall_1_lx, parameters.wall_1_ly, parameters.wall_1_lz );  
        const w2_len = new Point3D ( parameters.wall_2_lx, parameters.wall_2_ly, parameters.wall_2_lz );  
        const w3_len = new Point3D ( parameters.wall_3_lx, parameters.wall_3_ly, parameters.wall_3_lz );  
        const w1 = new Wall ( w1_pos, w1_len );
        const w2 = new Wall ( w2_pos, w2_len );
        const w3 = new Wall ( w3_pos, w3_len );
        const w8_pos = new Point3D ( (w1_pos.x + w2_pos.x)/2, (w1_pos.y + w2_pos.y)/2, (w1_pos.z + w1_len.z/2 + w2_pos.z + w2_len.z/2)/2 - parameters.baseboard_height/2 );
        const w8_len = new Point3D ( w1_len.x + w2_len.x, (w1_len.y + w2_len.y)/2, parameters.baseboard_height );
        const w8 = new Wall ( w8_pos, w8_len );
        const w9_pos = new Point3D ( (w1_pos.x + w2_pos.x)/2, (w1_pos.y + w2_pos.y)/2, (w1_pos.z - w1_len.z/2 + w2_pos.z - w2_len.z/2)/2 + parameters.baseboard_height/2 );
        const w9_len = new Point3D ( w1_len.x + w2_len.x, (w1_len.y + w2_len.y)/2, parameters.baseboard_height );
        const w9 = new Wall ( w9_pos, w9_len );
        const screen_pos = w3_pos.copy();
        screen_pos.y -= w3_len.y/2 + EPS;
        const screen = new Screen (screen_color, parameters.walls_color, screen_pos, w3_len.x, w3_len.z, parameters.screen_sampling_x, parameters.screen_threshold, parameters.screen_spot_radius, parameters.screen_refresh_time, parameters.screen_halo_size, parameters.screen_halo_smooth);
        const debugScreen = new Screen ("#FF0000", parameters.walls_color, screen_pos, w3_len.x, w3_len.z, parameters.screen_sampling_x, parameters.screen_threshold, parameters.screen_spot_radius, parameters.screen_refresh_time, parameters.screen_halo_size, parameters.screen_halo_smooth);
        return {
            walls: [w1,w2,w3,w8,w9],
            screen,
            debugScreen
        };
    }
    else if (number_of_slits == 2) {
        const w4_pos = new Point3D ( parameters.wall_4_pos_x, parameters.wall_4_pos_y, parameters.wall_4_pos_z );  
        const w5_pos = new Point3D ( parameters.wall_5_pos_x, parameters.wall_5_pos_y, parameters.wall_5_pos_z );  
        const w6_pos = new Point3D ( parameters.wall_6_pos_x, parameters.wall_6_pos_y, parameters.wall_6_pos_z );  
        const w7_pos = new Point3D ( parameters.wall_7_pos_x, parameters.wall_7_pos_y, parameters.wall_7_pos_z );  
        const w4_len = new Point3D ( parameters.wall_4_lx, parameters.wall_4_ly, parameters.wall_4_lz );  
        const w5_len = new Point3D ( parameters.wall_5_lx, parameters.wall_5_ly, parameters.wall_5_lz );  
        const w6_len = new Point3D ( parameters.wall_6_lx, parameters.wall_6_ly, parameters.wall_6_lz );  
        const w7_len = new Point3D ( parameters.wall_7_lx, parameters.wall_7_ly, parameters.wall_7_lz );  
        const w4 = new Wall ( w4_pos, w4_len );
        const w5 = new Wall ( w5_pos, w5_len );
        const w6 = new Wall ( w6_pos, w6_len );
        const w7 = new Wall ( w7_pos, w7_len );
        const w8_pos = new Point3D ( (w4_pos.x + w6_pos.x)/2, (w4_pos.y + w6_pos.y)/2, (w4_pos.z + w4_len.z/2 + w6_pos.z + w6_len.z/2)/2 - parameters.baseboard_height/2 );
        const w8_len = new Point3D ( w4_len.x + w6_len.x, (w4_len.y + w6_len.y)/2, parameters.baseboard_height );
        const w8 = new Wall ( w8_pos, w8_len );
        const w9_pos = new Point3D ( (w4_pos.x + w6_pos.x)/2, (w4_pos.y + w6_pos.y)/2, (w4_pos.z - w4_len.z/2 + w6_pos.z - w6_len.z/2)/2 + parameters.baseboard_height/2 );
        const w9_len = new Point3D ( w4_len.x + w6_len.x, (w4_len.y + w6_len.y)/2, parameters.baseboard_height );
        const w9 = new Wall ( w9_pos, w9_len );
        const screen_pos = w7_pos.copy();
        screen_pos.y -= w7_len.y/2 + EPS;
        let width = w7_len.x;
        const height = parameters.screen_height;
        if (radius_screen_limit) {
            const h = w7.position.y - w4.position.y;
            width = Math.sqrt (radius_screen_limit**2 - h**2) - parameters.screen_sampling_x;
        }
        const screen = new Screen (screen_color, parameters.walls_color, screen_pos, width, height, parameters.screen_sampling_x, parameters.screen_threshold, parameters.screen_spot_radius, parameters.screen_refresh_time, parameters.screen_halo_size, parameters.screen_halo_smooth);
        const debugScreen = new Screen ("#FF0000", parameters.walls_color, screen_pos, w7_len.x, w7_len.z, parameters.screen_sampling_x, parameters.screen_threshold, parameters.screen_spot_radius, parameters.screen_refresh_time, parameters.screen_halo_size, parameters.screen_halo_smooth);
        return {
            walls: [w4, w5, w6, w7, w8, w9],
            screen,
            debugScreen
        };
    }
    else throw new Error ("unsupported number of slits " + number_of_slits);
}

export class ParticleSimulation implements Simulation{

    // static parameters
    readonly _MARGIN = 10;
    
    private static_buffers: Buffers[] = [];
    private balls: Ball[] = [];
    private screen = new Screen ("#000000", "#FFFFFF", new Point3D (0,0,0), 0, 0, 0, 0, 0, 0, 0, 0);

    // configurable parameters
    private base_speed = new Point3D (0, 0.00001, 0.);
    private launching_pad_position = new Point3D (0,0,0);
    private balls_color = {r:1, g:1, b:1, a:1};
    private walls_color = {r:0, g:0, b:0, a:1};
    private walls: Wall[] = [];
    private parameters: SimulationParameters;

    constructor ( private gl: WebGLRenderingContext | WebGL2RenderingContext, private number_of_slits: number) {
        this.parameters = default_parameters;
        this.reset();
    }

    reset () {

        const {walls, screen} = compute_walls_depending_on_slits (this.number_of_slits, this.parameters, this.parameters.balls_color);
        this.walls = walls,
        this.screen = screen;

        this.balls_color = rgb_string_to_object (this.parameters.balls_color);
        this.walls_color = rgb_string_to_object (this.parameters.walls_color);

        this.launching_pad_position = new Point3D (this.parameters.launching_pad_pos_x, this.parameters.launching_pad_pos_y, this.parameters.launching_pad_pos_z);
        this.base_speed = new Point3D (this.parameters.balls_base_speed_x, this.parameters.balls_base_speed_y, this.parameters.balls_base_speed_z);

        this.static_buffers = this.walls.map ( w => initParallelepipedBuffers (this.gl, w.position, w.dimensions, this.walls_color) );
        this.static_buffers.push (initSphereBuffers (this.gl, this.launching_pad_position.copy(), this.parameters.balls_radius, this.balls_color));
        this.balls = [];
    }

    fire_callback () {
        const theta = Math.random()*2*Math.PI;
        const radius = this.parameters.cone_radius * Math.random();
        // assuming that the initial ball position is (0,0,0)
        // target for this ball is point (tx, 1, tz) choosen at random
        const tx = radius * Math.cos (theta);
        const tz = radius * Math.sin (theta);
        // ball speed is the vector (sx, by, sz) that points towards (tx, 1, tz), where by is the base speed y and sx,sz are computed proportionally to by
        const by = this.base_speed.y;
        const speed = new Point3D ( tx*by, by, tz*by );
        // console.debug ("new ball created: speed", speed);
        const ball = new Ball (
            this.launching_pad_position.copy(), 
            speed, 
            this.parameters.balls_radius,
            this.walls,
            this.parameters.balls_accel
        );
        this.balls.push ( ball );

    }

    get buffers () {
        return this.static_buffers.concat (this.balls.map ( b => initSphereBuffers (this.gl, b.position, b.radius, this.balls_color) ));
    }

    public tick ( deltat: number ) {
        this.balls.forEach ( b => b.tick(deltat) );
        this.balls = this.balls.filter ( 
            b => b.position.x < this._MARGIN && b.position.y < this._MARGIN && b.position.z < this._MARGIN 
        );
        const collisions = this.balls.map ( b => b.collided () );
        for (let i=collisions.length-1; i>=0; --i) {
            const c = collisions[i];
            if (c) {
                // adds the collisions to the static buffer array and removes them from the balls array
                this.static_buffers.push ( initSphereBuffers (this.gl, c, this.parameters.balls_radius, this.balls_color) );
                this.balls.splice (i, 1);
            }
        }
    }

    public parameters_changed_callback ( params: SimulationParameters ) {
        // console.log ("[SIMULATION] parameters changed callback: new parameters", this.parameters)
        this.parameters = params;
        this.reset ();
    }

}

// TODO: remove debugScreen and debug actions
export class WaveSimulation implements Simulation {

    private static_buffers: Buffers[] = [];
    private waves: WaveTree[] = [];
    private walls: Wall[] = [];
    private screen = new Screen ("#000000", "#FFFFFF", new Point3D (0,0,0), 0, 0, 0, 0, 0, 0, 0, 0 );
    private debug_screen = new Screen ("#FF0000", "#FFFFFF", new Point3D (0,0,0), 0, 0, 0, 0, 0, 0, 0, 0 );

    private generation = false;
    private screen_needs_to_be_update = true;
    private screen_update_counter = 0;

    private walls_color = {r:0, g:0, b:0, a:1};
    private waves_color = {r:0.5, g:0.5, b:1, a:1};
    private launching_pad_position = new Point3D (0,0,0);

    private slits: Point3D[] = [];
    private y_breaks: number[] = [];
    
    private _wave_visualization_mode = WaveVisualizationMode.CRESTS;

    private parameters: SimulationParameters;

    constructor ( private gl: WebGLRenderingContext | WebGL2RenderingContext, private number_of_slits: number) {
        this.parameters = default_parameters;
        this.reset();
    }

    reset () {

        this.generation = false;
        this.screen_needs_to_be_update = true;
        this.screen_update_counter = 0;
        
        this.launching_pad_position = new Point3D (this.parameters.launching_pad_pos_x, this.parameters.launching_pad_pos_y, this.parameters.launching_pad_pos_z);

        const {walls, screen, debugScreen} = compute_walls_depending_on_slits (this.number_of_slits, this.parameters, this.parameters.waves_color, this.parameters.waves_max_distance);
        this.walls = walls;
        this.screen = screen;
        this.debug_screen = debugScreen;

        if (this.number_of_slits == 1) {
            const w1 = this.walls[0], w2 = this.walls[1], w3 = this.walls[2]; 
            this.slits = [new Point3D ((w2.position.x + w2.dimensions.x/2 + w1.position.x - w1.dimensions.x/2)/2, w1.position.y, w1.position.z)];
            this.y_breaks = [this.launching_pad_position.y, w1.position.y, w3.position.y];
        }
        else if (this.number_of_slits == 2) {
            const w4 = this.walls[0], w5 = this.walls[1], w6 = this.walls[2], w7 = this.walls[3];
            this.slits = [
                new Point3D ((w6.position.x + w6.dimensions.x/2 + w5.position.x - w5.dimensions.x/2)/2, w5.position.y, w5.position.z),
                new Point3D ((w5.position.x + w5.dimensions.x/2 + w4.position.x - w4.dimensions.x/2)/2, w4.position.y, w4.position.z)
            ];
            this.y_breaks = [this.launching_pad_position.y, w5.position.y, w7.position.y];
        }
        else throw new Error ("unsupported number of slits " + this.number_of_slits);

        // console.log ("[WaveSimulation] reset. walls", this.walls, "slits", this.slits, "ybreaks", this.y_breaks);

        this.waves_color = rgb_string_to_object (this.parameters.waves_color);
        this.walls_color = rgb_string_to_object (this.parameters.walls_color);

        this.static_buffers = this.walls.map ( w => initParallelepipedBuffers (this.gl, w.position, w.dimensions, this.walls_color) );
        this.static_buffers.push (initSphereBuffers (this.gl, this.launching_pad_position.copy(), this.parameters.balls_radius, this.waves_color));
        this.waves = [];

    }

    public fire_callback () {
        this.generation = !this.generation;
        if (this.generation) {
            this.waves.unshift ( new WaveTree (
                this.launching_pad_position.copy(), this.parameters.waves_speed, this.parameters.waves_wavelength,
                copy_array(this.slits), this.y_breaks, this.parameters.waves_max_distance
            ));
            // console.debug ("[Wave-simulation] generated wave:", this.waves[0]);
        }
    }

    private tick_debug () {
        if (this.waves.length > 0 ) {
            const crests = this.waves[this.waves.length-1].crests;
            const crests_A = crests.filter ( c => c.center.x < this.launching_pad_position.x - 0.001 );
            const crests_B = crests.filter ( c => c.center.x > this.launching_pad_position.x + 0.001 );
            if (crests_A.length >= 10 && crests_B.length >= 10) {
                for (let i=0; i<4; ++i) {
                    for (let j=0; j<4; ++j) {
                        const ip = circle_circle_intersection (crests_A[i], crests_B[j]).filter (p => p.y>crests_A[i].center.y)[0];
                        this.debug_screen.add_spot (ip);
                    }
                }
            }
        }
    }

    public tick ( deltat: number ) {
        if (this.waves.length) {
            for (let j=this.waves.length-1; j>=1; --j) {
                const dead = this.waves[j].tick (deltat, false);
                if (dead) {
                    this.waves.splice (j,1);
                }
            }
            const dead = this.waves[0].tick (deltat, this.generation);
            if (dead) {
                this.waves.splice (0,1);
            }
        }
        if (this.screen_needs_to_be_update && this.waves.length) {
            const screen_values = this.waves[this.waves.length-1].get_line_values (this.screen.y, this.screen.xmin, this.screen.xmax, this.parameters.screen_sampling_x);
            const update_required = this.screen.update (screen_values);
            if (update_required) {
                this.screen_update_counter = 0;
            }
            else {
                ++this.screen_update_counter;
                if (this.screen_update_counter >= this.parameters.screen_stop_updating_after_how_many_refreshes_not_required) {
                    this.screen_needs_to_be_update = false;
                }
            }
        }
        // this.tick_debug ();
    }

    get buffers () {
        // DEBUG
        // const debugScreenBuffers = this.debug_screen.get_buffers (this.gl);
        const screenBuffers: Buffers[] = this.screen.get_buffers (this.gl);
        if (this._wave_visualization_mode == WaveVisualizationMode.CRESTS) {
            const crestsBuffers = this.waves.map ( w => {
                const crests = w.crests;
                return ([] as Buffers[]).concat (...crests.map (c => 
                    initLimitedCircleBuffers (this.gl, c.center, c.radius, this.waves_color, c.ymin, c.ymax )));
            });
            return this.static_buffers.concat (...crestsBuffers, screenBuffers, 
                // debugScreenBuffers
            );
        }
        else {
            return this.static_buffers.concat(screenBuffers);
        }

    }

    public parameters_changed_callback ( params: SimulationParameters ) {
        this.parameters = params;
        this.reset ();
    }
}

export class LightSimulation implements Simulation{

    private _INTERFERENCE_VALUES_DOUBLE_SLIT = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];
    private _INTERFERENCE_VALUES_SINGLE_SLIT = [
        0, 
    ];
    private parameters: SimulationParameters;
    private static_buffers: Buffers[] = [];
    private cone_buffers: Buffers = {position: [], color: [], number_of_vertices:0};
    private walls: Wall[] = [];
    private walls_color = {r:0, g:0, b:0, a:0};
    private light_color = {r:0, g:0, b:0, a:0};
    private launching_pad_position = new Point3D (0,0,0);
    private screen = new Screen ("#000000", "#FFFFFF", new Point3D (0,0,0), 0, 0, 0, 0, 0, 0, 0, 0 );
    private is_light_on = false;
    
    constructor ( private gl: WebGLRenderingContext | WebGL2RenderingContext, private number_of_slits: number) {
        this.parameters = default_parameters;
        this.reset();
    }

    reset () {

        // TODO: remove screen_limit and draw two different interference patterns (for single and double slit)
        const {walls, screen} = compute_walls_depending_on_slits (this.number_of_slits, this.parameters, this.parameters.light_color, this.parameters.waves_max_distance);
        this.walls = walls,
        this.screen = screen;

        this.walls_color = rgb_string_to_object (this.parameters.walls_color);
        this.light_color = rgb_string_to_object (this.parameters.light_color);

        this.launching_pad_position = new Point3D (this.parameters.launching_pad_pos_x, this.parameters.launching_pad_pos_y, this.parameters.launching_pad_pos_z);

        this.static_buffers = this.walls.map ( w => initParallelepipedBuffers (this.gl, w.position, w.dimensions, this.walls_color) );
        this.static_buffers.push (initSphereBuffers (this.gl, this.launching_pad_position.copy(), this.parameters.balls_radius, this.light_color));

        const cone_height = this.walls[0].position.y - this.launching_pad_position.y;
        const cone_radius = this.parameters.cone_radius * cone_height;
        this.cone_buffers = initConeBuffer (this.gl, this.launching_pad_position, cone_height, cone_radius, this.light_color);
    }

    public fire_callback () {
        this.is_light_on = !this.is_light_on;
    }
    
    get buffers () {
        const screenBuffers = this.screen.get_buffers (this.gl);
        const lightBuffers = this.is_light_on ? [this.cone_buffers] : [];
        return this.static_buffers.concat (...screenBuffers, ...lightBuffers);
    }
    
    tick(deltat: number): void {
        if (this.is_light_on) {
            this.screen.update ( this.number_of_slits == 1 ? this._INTERFERENCE_VALUES_SINGLE_SLIT : this._INTERFERENCE_VALUES_DOUBLE_SLIT );
        }
    }
    
    parameters_changed_callback(pars: SimulationParameters): void {
        this.parameters = pars;
        this.reset ();
    }
}

export class ElectronsSimulation implements Simulation{

    private _MAX_FIRABLE_ELECTRONS = 5_000;

    private parameters: SimulationParameters;
    private static_buffers: Buffers[] = [];
    private walls: Wall[] = [];
    private walls_color = {r:0, g:0, b:0, a:0};
    private electrons_color = {r:0, g:0, b:0, a:0};
    private launching_pad_position = new Point3D (0,0,0);
    private screen = new Screen ("#000000", "#FFFFFF", new Point3D (0,0,0), 0, 0, 0, 0, 0, 0, 0, 0 );

    private stripe_std = 0;
    private n_stripes = 0;
    private base_x = 0;

    private generation = false;
    private ms_per_electron = 0;
    private spare_ms = 0;
    private total_electrons_fired = 0;

    private statistics: {[stripe: number]: number} = {};
    
    constructor ( private gl: WebGLRenderingContext | WebGL2RenderingContext, private number_of_slits: number) {
        this.parameters = default_parameters;
        this.reset();
    }

    reset () {

        const {walls, screen} = compute_walls_depending_on_slits (this.number_of_slits, this.parameters, this.parameters.electrons_color);
        this.walls = walls,
        this.screen = screen;

        this.walls_color = rgb_string_to_object (this.parameters.walls_color);
        this.electrons_color = rgb_string_to_object (this.parameters.electrons_color);

        this.launching_pad_position = new Point3D (this.parameters.launching_pad_pos_x, this.parameters.launching_pad_pos_y, this.parameters.launching_pad_pos_z);

        this.static_buffers = this.walls.map ( w => initParallelepipedBuffers (this.gl, w.position, w.dimensions, this.walls_color) );
        this.static_buffers.push (initSphereBuffers (this.gl, this.launching_pad_position.copy(), this.parameters.balls_radius, this.electrons_color));

        this.stripe_std = this.parameters.electrons_stripes_distance*this.parameters.electrons_stripes_std_factor;
        this.n_stripes = Math.floor(this.screen.w / this.parameters.electrons_stripes_distance) + 1;
        this.base_x = (this.screen.w - (this.n_stripes-1)*this.parameters.electrons_stripes_distance) / 2 + this.screen.xmin;
    
        this.spare_ms = 0;
        this.ms_per_electron = 1000/this.parameters.electrons_per_second;
        this.total_electrons_fired = 0;

        this.statistics = {};
    }

    public fire_callback () {
        this.generation = !this.generation;
        console.debug ("[electrons-simulation] fire callback statistics:", this.statistics);
    }
    
    get buffers () {
        const screenBuffers = this.screen.get_buffers (this.gl);
        return this.static_buffers.concat (...screenBuffers);
    }
    
    private select_random_stripe () {
        let out: number;
        if (this.number_of_slits == 1) {
            out = Math.floor (this.n_stripes/2);
        }
        else {
            const max_rand = Math.ceil(this.n_stripes/2);
            const r = Math.random() * max_rand * (max_rand + 1) / 2;
            const p = Math.floor ( (-1 + Math.sqrt(1+8*r))/2 );
            out = Math.random() < 0.5 ? p : this.n_stripes - p - 1;
        }
        this.statistics[out] = (this.statistics[out] || 0) + 1;
        return out;
    }

    tick(deltat: number): void {
        if (this.generation && this.total_electrons_fired < this._MAX_FIRABLE_ELECTRONS) {
            let total_time = this.spare_ms + deltat;
            while (total_time > this.ms_per_electron) {
                const chosen_stripe = this.select_random_stripe ();
                const mean = this.base_x + this.parameters.electrons_stripes_distance * chosen_stripe;
                const std = this.stripe_std/(Math.abs((this.n_stripes-1)/2-chosen_stripe)+0.5);
                const x = random_gauss (mean, std);
                const z = this.screen.zmin + this.screen.h * Math.random ();
                this.screen.add_spot ({x,z});
                total_time -= this.ms_per_electron;
                ++this.total_electrons_fired;
            }
            this.spare_ms = total_time;
        }
    }
    
    parameters_changed_callback(pars: SimulationParameters): void {
        this.parameters = pars;
        this.reset ();
    }
}
