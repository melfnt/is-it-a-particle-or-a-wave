
import {Buffers} from './rendering';

function create_buffer_or_else_throw (gl: WebGLRenderingContext | WebGL2RenderingContext, message="could not initialize buffer"): WebGLBuffer {
    const buf = gl.createBuffer();
    if ( buf == null ) throw new Error (message);
    return buf;
}

function javascript_arrays_to_gl_buffers ( gl: WebGLRenderingContext | WebGL2RenderingContext, 
                                           positions: number[], colors: number[], indices?: number[],
                                           mode?: GLenum ): Buffers {
    const positionBuffer = create_buffer_or_else_throw(gl);
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,
                  new Float32Array(positions),
                  gl.STATIC_DRAW);
              
    const colorBuffer = create_buffer_or_else_throw (gl);
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    let indexBuffer: WebGLBuffer | undefined;
    if (indices) {
      indexBuffer = create_buffer_or_else_throw (gl);
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
      gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
    }

    return {
      position: positionBuffer,
      number_of_vertices: indices? indices.length : positions.length/3,
      color: colorBuffer,
      indices: indexBuffer,
      mode
    };
}

export function initParallelepipedBuffers (gl: WebGLRenderingContext | WebGL2RenderingContext, center={x:0, y:0, z:0}, side={x:1, y:1, z:1}, color={r:0, g:0, b:0, a:1}): Buffers
{
    // create an array of positions for the cube.
    const positions = [
      // Front face
      center.x-0.5*side.x, center.y-0.5*side.y, center.z+0.5*side.z,
      center.x+0.5*side.x, center.y-0.5*side.y, center.z+0.5*side.z,
      center.x+0.5*side.x, center.y+0.5*side.y, center.z+0.5*side.z,
      center.x-0.5*side.x, center.y+0.5*side.y, center.z+0.5*side.z,

      // Back face
      center.x-0.5*side.x, center.y-0.5*side.y, center.z-0.5*side.z,
      center.x-0.5*side.x, center.y+0.5*side.y, center.z-0.5*side.z,
      center.x+0.5*side.x, center.y+0.5*side.y, center.z-0.5*side.z,
      center.x+0.5*side.x, center.y-0.5*side.y, center.z-0.5*side.z,

      // Top face
      center.x-0.5*side.x, center.y+0.5*side.y, center.z-0.5*side.z,
      center.x-0.5*side.x, center.y+0.5*side.y, center.z+0.5*side.z,
      center.x+0.5*side.x, center.y+0.5*side.y, center.z+0.5*side.z,
      center.x+0.5*side.x, center.y+0.5*side.y, center.z-0.5*side.z,

      // Bottom face
      center.x-0.5*side.x, center.y-0.5*side.y, center.z-0.5*side.z,
      center.x+0.5*side.x, center.y-0.5*side.y, center.z-0.5*side.z,
      center.x+0.5*side.x, center.y-0.5*side.y, center.z+0.5*side.z,
      center.x-0.5*side.x, center.y-0.5*side.y, center.z+0.5*side.z,

      // Right face
      center.x+0.5*side.x, center.y-0.5*side.y, center.z-0.5*side.z,
      center.x+0.5*side.x, center.y+0.5*side.y, center.z-0.5*side.z,
      center.x+0.5*side.x, center.y+0.5*side.y, center.z+0.5*side.z,
      center.x+0.5*side.x, center.y-0.5*side.y, center.z+0.5*side.z,

      // Left face
      center.x-0.5*side.x, center.y-0.5*side.y, center.z-0.5*side.z,
      center.x-0.5*side.x, center.y-0.5*side.y, center.z+0.5*side.z,
      center.x-0.5*side.x, center.y+0.5*side.y, center.z+0.5*side.z,
      center.x-0.5*side.x, center.y+0.5*side.y, center.z-0.5*side.z,
    ];

    const dr = 0.2;
    const dg = 0.2;
    const db = 0.2;

    // Now set up the colors for the faces. We'll use solid colors
    // for each face.
    const faceColors = [
      [color.r-dr,  color.g-dg,  color.b+db,  color.a],
      [color.r+dr,  color.g+dg,  color.b+db,  color.a],
      [color.r-dr,  color.g+dg,  color.b+db,  color.a],
      [color.r,     color.g,     color.b,     color.a],
      [color.r-dr,  color.g-dg,  color.b-db,  color.a],
      [color.r+dr,  color.g-dg,  color.b-db,  color.a],
    ];

    // Convert the array of colors into a table for all the vertices.

    var colors: any[] = [];

    for (var j = 0; j < faceColors.length; ++j) {
      const c = faceColors[j];

      // Repeat each color four times for the four vertices of the face
      colors = colors.concat(c, c, c, c);
    }

    const indices = [
      0,  1,  2,      0,  2,  3,    // front
      4,  5,  6,      4,  6,  7,    // back
      8,  9,  10,     8,  10, 11,   // top
      12, 13, 14,     12, 14, 15,   // bottom
      16, 17, 18,     16, 18, 19,   // right
      20, 21, 22,     20, 22, 23,   // left
    ];

    return javascript_arrays_to_gl_buffers (gl, positions, colors, indices);

}

export function initSphereBuffers (gl: WebGLRenderingContext | WebGL2RenderingContext, center={x:0, y:0, z:0}, radius=1, facesColor={r:1,g:1,b:1,a:1}): Buffers
{
    const phi = (1.0 + Math.sqrt(5.0)) / 2.0;

    const positions = [
        center.x-radius, center.y+phi*radius, center.z,
        center.x+radius, center.y+phi*radius, center.z,
        center.x-radius, center.y-phi*radius, center.z,
        center.x+radius, center.y-phi*radius, center.z,

        center.x, center.y-radius, center.z+phi*radius,
        center.x, center.y+radius, center.z+phi*radius,
        center.x, center.y-radius, center.z-phi*radius,
        center.x, center.y+radius, center.z-phi*radius,

        center.x+phi*radius, center.y, center.z-radius,
        center.x+phi*radius, center.y, center.z+radius,
        center.x-phi*radius, center.y, center.z-radius,
        center.x-phi*radius, center.y, center.z+radius,
    ];

    const colors: number[] = [];

    for (var j = 0; j < positions.length/3; ++j) {
      colors.push(
        facesColor.r + 0.1 * Math.random(), 
        facesColor.g + 0.1 * Math.random(),
        facesColor.b + 0.1 * Math.random(),
        facesColor.a
      );
    }

    const indices = [
        // 5 faces around point 0
        0, 11, 5,
        0, 5, 1,
        0, 1, 7,
        0, 7, 10,
        0, 10, 11,

        // 5 adjacent faces
        1, 5, 9,
        5, 11, 4,
        11, 10, 2,
        10, 7, 6,
        7, 1, 8,

        // 5 faces around point 3
        3, 9, 4,
        3, 4, 2,
        3, 2, 6,
        3, 6, 8,
        3, 8, 9,

        // 5 adjacent faces
        4, 9, 5,
        2, 4, 11,
        6, 2, 10,
        8, 6, 7,
        9, 8, 1,
    ];

    return javascript_arrays_to_gl_buffers (gl, positions, colors, indices);
}

export function initCircleBuffer (gl: WebGLRenderingContext | WebGL2RenderingContext, center={x:0, y:0, z:0}, radius=1, color={r:0,g:0,b:0,a:0}): Buffers {
 
    const colors: number[] = [];
    const positions: number[] = [];
    for (let j=0; j<360; ++j) {
        const rad = j*Math.PI/180;
        const dx = radius*Math.sin(rad);
        const dy = radius*Math.cos(rad);
        positions.push (center.x+dx, center.y+dy, center.z);
        colors.push (color.r, color.g, color.b, color.a);
    }

    return javascript_arrays_to_gl_buffers (gl, positions, colors);
}

export function initPolygonBuffer (gl: WebGLRenderingContext | WebGL2RenderingContext, vertices: {x:number, y:number, z:number}[], colors:{r:number,g:number,b:number,a:number}[], fill=false): Buffers {
 
    const colors_array: number[] = [];
    const positions: number[] = [];
    for (let j=0; j<vertices.length; ++j) {  
        positions.push (vertices[j].x, vertices[j].y, vertices[j].z);
        colors_array.push (colors[j].r, colors[j].g, colors[j].b, colors[j].a);
    }

    // console.debug ("[SHAPES] init polygonal buffer: positions", positions, "colors", colors_array);

    return javascript_arrays_to_gl_buffers (gl, positions, colors_array, undefined, fill? gl.TRIANGLE_FAN : gl.LINE_LOOP);
}

export function initLimitedCircleBuffers (gl: WebGLRenderingContext | WebGL2RenderingContext, center={x:0, y:0, z:0}, radius=1, 
                                  color={r:0,g:0,b:0,a:0}, ymin: number | null = null, ymax: number | null = null): Buffers[] {
 
    const out: Buffers[] = [];
    let colors: number[] = [];
    let positions: number[] = [];
    // loop from -90 to 270 to avoid unnecessary breaking waves at ymax (0 degree).
    for (let j=-90; j<270; ++j) {
        const rad = j*Math.PI/180;
        const px = center.x + radius*Math.sin(rad);
        const py = center.y + radius*Math.cos(rad);
        if ( (ymin === null || py>=ymin) && (ymax === null || py<=ymax) ) {
          positions.push (px, py, center.z);
          colors.push (color.r, color.g, color.b, color.a);  
        }
        else if (positions.length) {
          out.push (javascript_arrays_to_gl_buffers(gl, positions, colors));

          positions = [];
          colors = [];
          
        }
    }

    if (positions.length) {
      out.push (javascript_arrays_to_gl_buffers(gl, positions, colors));
    }

    return out;
}

// radius is the radius at height=1. The actual base radius is computed depending on the actual height
export function initConeBuffer (gl: WebGLRenderingContext | WebGL2RenderingContext, apex={x:0, y:0, z:0}, height=1, radius=1, color={r:0,g:0,b:0,a:0}): Buffers {

  // positions[0,1,2] are the center coordinates
  // positions[3::360*3] are the coordinates of the vertexes of the circle
  const positions = [];
  const colors = [];
    
  positions.push (apex.x, apex.y, apex.z);
  colors.push (color.r + 0.1*Math.random(), color.g + 0.1*Math.random(), color.b + 0.1*Math.random(), color.a);
  const A = {x: apex.x, y: apex.y+height, z: apex.z};
  const dc = -1;
  for (let j=0; j<360; ++j) {
    const rad = j*Math.PI/180;
    const dx = radius*Math.cos(rad);
    const dz = radius*Math.sin(rad);
    positions.push (A.x+dx, A.y, A.z+dz);
    colors.push (color.r - j*dc/360, color.g - j*dc/360, color.b + j*dc/360, color.a);
  }

  const indices = [];
  for (let j=1; j<360; ++j) {
    indices.push (j, 0, j+1);
  }
  indices.push (360,0,1);

  return javascript_arrays_to_gl_buffers (gl, positions, colors, indices);
  
}
