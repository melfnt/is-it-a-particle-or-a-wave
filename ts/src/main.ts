import { RenderingManager } from "./rendering";
import { Controls3D } from "./controls";
import { Simulation, ParticleSimulation, WaveSimulation, LightSimulation, ElectronsSimulation } from "./Simulation";
import { ParametersManager } from "./parameters";

enum Throwable {
  PARTICLES = "particles",
  WAVES = "waves",
  ELECTRONS = "electrons",
  LIGHT = "light",
}

let number_of_slits = 1;
let thrown_object: Throwable = Throwable.PARTICLES;
let simulation: Simulation;
let registered_event_listener: any;
let gl: WebGLRenderingContext | WebGL2RenderingContext;
let canvas: HTMLCanvasElement;
let parameters_manager: ParametersManager;
const rendering_manager = new RenderingManager();
let is_pause = false;
let automatic_fire_handle: number | null = null;


main_interaction();
main_gl();

function show_hide_dom_elements ( ids_show: string[], ids_hide: string[], opposite?:boolean) {
  if (opposite) {
    const tmp = ids_show;
    ids_show = ids_hide;
    ids_hide = tmp;
  }
  ids_show.forEach (id => {
    const elm = document.getElementById (id);
    elm?.classList.remove ("hidden");
  });
  ids_hide.forEach (id => {
    const elm = document.getElementById (id);
    elm?.classList.add ("hidden");
  });
}

function reset_simulation () {
  if (automatic_fire_handle !== null){
    clearInterval (automatic_fire_handle);
    automatic_fire_handle = null;
  }
  if (simulation) parameters_manager.unregister (simulation);
  if (registered_event_listener) canvas.removeEventListener ("keypress", registered_event_listener );
  is_pause = false;
  show_hide_dom_elements (["pause-control"], ["play-control"]);
  switch (thrown_object) {
    case Throwable.PARTICLES:
      simulation = new ParticleSimulation (gl, number_of_slits);
      break;
    case Throwable.WAVES:
      simulation = new WaveSimulation (gl, number_of_slits);
      break;
    case Throwable.LIGHT:
      simulation = new LightSimulation (gl, number_of_slits);
      break;
    case Throwable.ELECTRONS:
      simulation = new ElectronsSimulation (gl, number_of_slits);
      break;
    default:
      throw new Error ("unsupported thrown object: " + thrown_object);
  }
  registered_event_listener = keypress_callback (simulation.fire_callback.bind(simulation));
  canvas.addEventListener ("keypress", registered_event_listener );
  canvas.focus();
  parameters_manager.register (simulation);
}

function toggle_automatic_fire ( fire_callback: Function ) {
  if (automatic_fire_handle === null) {
    automatic_fire_handle = setInterval(fire_callback, 1000/parameters_manager.parameters.parameters.automatic_fire_balls_per_second);
  }
  else {
    clearInterval (automatic_fire_handle);
    automatic_fire_handle = null;
  }
}

function keypress_callback (fire_callback: Function): (this: HTMLCanvasElement, ev: KeyboardEvent) => any {
    return (evt: KeyboardEvent) => {
        const pressed = evt.key.toLowerCase();
        if (!is_pause) {
          if (thrown_object === Throwable.PARTICLES) {
              if (pressed == "f") {
                fire_callback ();
              }
              if (pressed == " " )
              {
                toggle_automatic_fire ( fire_callback );
              }
          }
          else {
            if (pressed == " ") {
              fire_callback ();
            }
          }
        }
        if ( pressed === "p" ) {
          is_pause = !is_pause;
          show_hide_dom_elements (["play-control"], ["pause-control"], !is_pause);
        }
        if ( pressed === "r" ) {
          reset_simulation ();
        }
        if ( pressed === " " ) {
          evt.preventDefault();
        }
    }
}

function main_gl() {
    canvas = <HTMLCanvasElement> document.querySelector('#glcanvas');
    gl = canvas.getContext('webgl', {premultipliedAlpha: false}) || canvas.getContext('experimental-webgl', {premultipliedAlpha: false}) as WebGL2RenderingContext;

    // If we don't have a GL context, give up now
    if (!gl) {
      throw new Error ('Unable to initialize WebGL. Your browser or machine may not support it.');
    }

    const controls = new Controls3D (canvas);
    const programInfo = rendering_manager.setup (gl);

    reset_simulation ();

    let then = 0;
    let last_paint = 0;
    let frames = 0;

    // update simulation and draw the scene repeatedly
    function render(now: number) {
      if (now > last_paint+1000) {
        if (frames < 20) console.warn ("[RENDER] less than 20 fps!");
        frames = 0;
        last_paint = now;
      }
      ++frames;

      const deltaTime = then == 0 ? 0 : now - then;
      then = now;

      if (!is_pause) {
        simulation.tick ( deltaTime );
      }
      
      rendering_manager.drawScene(gl, programInfo, simulation.buffers, controls.modelViewMatrix, deltaTime);

      requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
}

function settings_changed (this: HTMLElement, evt: Event) {
  const old_number_of_slits = number_of_slits;
  const old_thrown_objects = thrown_object;
  switch (this.id) {
    case 'single-slit':
      number_of_slits = 1;
      break;
      
    case 'double-slit':
      number_of_slits = 2;
      break;
      
    case 'throw-particles':
      thrown_object = Throwable.PARTICLES;
      break;
    
    case 'throw-waves':
      thrown_object = Throwable.WAVES;
      break;
    
    case 'throw-light':
      thrown_object = Throwable.LIGHT;
      break;
    
    case 'throw-electrons':
      thrown_object = Throwable.ELECTRONS;
      break;
  }

  if ( old_number_of_slits != number_of_slits || old_thrown_objects != thrown_object ) {
    reset_simulation();

    const parent = this.parentElement;
    if (parent) {
      const siblings = parent.querySelectorAll (".setting");
      siblings.forEach (e => e.classList.remove("selected"));
    }
    this.classList.add ("selected");

    for ( let possible_throw in Throwable ) {
      const elm = document.getElementById ( possible_throw.toLowerCase() + "-controls" );
      elm?.classList.add ("hidden");
    }
    const elm = document.getElementById ( thrown_object.toLowerCase() + "-controls" );
    elm?.classList.remove ("hidden");

  }

  canvas.focus();
  
}

function main_interaction() {
  const parameters_container = document.getElementById ("parameters-container");
  if (!parameters_container) throw new Error ("Parameters container not found");
  const textarea_elm = document.getElementById ("json-parameters");
  if (!textarea_elm) throw new Error ("Json parameters textarea not found");
  parameters_manager = new ParametersManager (parameters_container, <HTMLInputElement>textarea_elm);

  parameters_manager.register (rendering_manager);
  const settings_elms = document.getElementsByClassName ("setting");
  for (let j=0; j<settings_elms.length; ++j) {
    settings_elms[j].addEventListener ("click", settings_changed);
  }
  
}

