
import {mat4} from "gl-matrix";
import {Parametrizable, SimulationParameters} from './parameters';
import { rgb_string_to_object } from "./utils";

export interface Buffers {
    position: WebGLBuffer;
    color: WebGLBuffer;
    indices?: WebGLBuffer;
    mode?: GLenum;
    number_of_vertices: number;
}

//TODO: maybe save gl instead of passing it as a parameters (?)
export class RenderingManager implements Parametrizable {

    private background_color = {r:1, g:1, b:1, a:1};

    public setup ( gl: WebGLRenderingContext | WebGL2RenderingContext ) {
        // Vertex shader program
        const vsSource = `
            attribute vec4 aVertexPosition;
            attribute vec4 aVertexColor;

            uniform mat4 uModelViewMatrix;
            uniform mat4 uProjectionMatrix;

            varying lowp vec4 vColor;

            void main(void) {
                gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
                vColor = aVertexColor;
            }
        `;

        // Fragment shader program
        const fsSource = `
            varying lowp vec4 vColor;

            void main(void) {
                gl_FragColor = vColor;
            }
        `;

        // Initialize a shader program; this is where all the lighting
        // for the vertices and so forth is established.
        const shaderProgram = this.initShaderProgram(gl, vsSource, fsSource);

        // Collect all the info needed to use the shader program.
        // Look up which attributes our shader program is using
        // for aVertexPosition, aVevrtexColor and also
        // look up uniform locations.
        const programInfo = {
            program: shaderProgram,
            attribLocations: {
                vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
                vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor'),
            },
            uniformLocations: {
                projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
                modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
            },
        };

        return programInfo;
    }

    //
    // Initialize a shader program, so WebGL knows how to draw our data
    //
    private initShaderProgram(gl: WebGLRenderingContext | WebGL2RenderingContext, vsSource: string, fsSource: string) {
        const vertexShader = this.loadShader(gl, gl.VERTEX_SHADER, vsSource);
        const fragmentShader = this.loadShader(gl, gl.FRAGMENT_SHADER, fsSource);
        // Create the shader program
        const shaderProgram = gl.createProgram();
        if (shaderProgram == null) throw new Error("Cannot initialize shader program");
    
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);
    
        // If creating the shader program failed, alert
        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        throw new Error ('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        }
    
        return shaderProgram;
    }

    //
    // creates a shader of the given type, uploads the source and
    // compiles it.
    //
    private loadShader(gl: WebGLRenderingContext | WebGL2RenderingContext, type: number, source: string) {
        const shader = gl.createShader(type);
        if (shader == null) throw new Error ("cannot create shader");    
        gl.shaderSource(shader, source);
        gl.compileShader(shader);
    
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            gl.deleteShader(shader);
            throw new Error ("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
        }
    
        return shader;
    }

    //
    // Draw the scene.
    //
    public drawScene(gl: WebGLRenderingContext | WebGL2RenderingContext, programInfo: any, buffers_array: Buffers[], modelViewMatrix: mat4, deltaTime: number) {
        gl.clearColor(this.background_color.r, this.background_color.g, this.background_color.b, this.background_color.a); 
        gl.clearDepth(1.0);                 // Clear everything
        gl.enable(gl.DEPTH_TEST);           // Enable depth testing
        gl.depthFunc(gl.LEQUAL);            // Near things obscure far things
    
        // Clear the canvas before we start drawing on it.  
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
        // Create a perspective matrix, a special matrix that is
        // used to simulate the distortion of perspective in a camera.
        // Our field of view is 45 degrees, with a width/height
        // ratio that matches the display size of the canvas
        // and we only want to see objects between 0.1 units
        // and 100 units away from the camera.
    
        const fieldOfView = 45 * Math.PI / 180;   // in radians
        const canvas = gl.canvas as HTMLCanvasElement;
        const aspect = canvas.clientWidth / canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100.0;
        const projectionMatrix = mat4.create();
    
        mat4.perspective(projectionMatrix,
                        fieldOfView,
                        aspect,
                        zNear,
                        zFar);
        
        buffers_array.forEach ( (buffers) => {

            // Tell WebGL how to pull out the positions from the position
            // buffer into the vertexPosition attribute
            {
                const numComponents = 3;
                const type = gl.FLOAT;
                const normalize = false;
                const stride = 0;
                const offset = 0;
                gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
                gl.vertexAttribPointer(
                    programInfo.attribLocations.vertexPosition,
                    numComponents,
                    type,
                    normalize,
                    stride,
                    offset);
                gl.enableVertexAttribArray(
                    programInfo.attribLocations.vertexPosition);
            }

            // Tell WebGL how to pull out the colors from the color buffer
            // into the vertexColor attribute.
            {
                const numComponents = 4;
                const type = gl.FLOAT;
                const normalize = false;
                const stride = 0;
                const offset = 0;
                gl.bindBuffer(gl.ARRAY_BUFFER, buffers.color);
                gl.vertexAttribPointer(
                    programInfo.attribLocations.vertexColor,
                    numComponents,
                    type,
                    normalize,
                    stride,
                    offset);
                gl.enableVertexAttribArray(
                    programInfo.attribLocations.vertexColor);
            }

            if ( buffers.indices ) {
                // Tell WebGL which indices to use to index the vertices
                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);
            }
            
            // Tell WebGL to use our program when drawing
            gl.useProgram(programInfo.program);
        
            // Set the shader uniforms
        
            gl.uniformMatrix4fv(
                programInfo.uniformLocations.projectionMatrix,
                false,
                projectionMatrix);
            gl.uniformMatrix4fv(
                programInfo.uniformLocations.modelViewMatrix,
                false,
                modelViewMatrix);
        
            {
                const mode: GLenum = (buffers.mode !== undefined) ? buffers.mode : (buffers.indices ? gl.TRIANGLES : gl.LINE_STRIP);
                const type = gl.UNSIGNED_SHORT;
                const offset = 0;
                if (buffers.indices) {
                    gl.drawElements(mode, buffers.number_of_vertices, type, offset);
                }
                else {
                    gl.drawArrays (mode, offset, buffers.number_of_vertices);
                }
            }

        });

    }

    parameters_changed_callback (param: SimulationParameters) {
        this.background_color = rgb_string_to_object (param.background_color);
    }

}
