# Physics simulation to show the difference between particles and waves.

bootstrapped from [ts-demo-webpack](https://github.com/rauschma/ts-demo-webpack) and [webgl-examples](https://github.com/mdn/webgl-examples). [webgl documentation](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API).

# Setup

Clone this repository, then install npm dependencies:

    git clone https://bitbucket.org/melfnt/is-it-a-particle-or-a-wave.git
    cd is-it-a-particle-or-a-wave
    npm install
    
# Develop

Run

    npm run dev

The webpage(s) will be available at `http://127.0.0.1:8080`. Every time you edit the html files into the `html` folder and/or the typescript files into the `ts` folder the application will be automatically rebuilt: reload the page to see the the changes.

# Production build

Run

    npm run build

For a production build. The buold pages will be available in the `dist/` directory.

# License

GNU GENERAL PUBLIC LICENSE Version 3, see the file `LICENSE.md`
